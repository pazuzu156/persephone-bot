module persephone

go 1.12

require (
	github.com/andersfylling/disgord v0.11.3
	github.com/polaron/aurora v0.1.0
	github.com/shkh/lastfm-go v0.0.0-20180729041255-29483feae857
)
