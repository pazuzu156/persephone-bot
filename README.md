# Persephone (Working Title)

Persephone is a Discord bot that's used to interface with Lastfm, and is built to be used with the Untrodden Corrodors of Hades Discord server.

## Used Libraries

Libraries used for the bot will be listed here (you can also see them listed in `go.mod`)

* [Disgord](https://github.com/andersfylling/disgord) The Discord library for Go
* [Aurora](https://github.com/polaron/aurora) The command router for Disgord
* [Lastfm-Go](https://github.com/shkh/lastfm-go) The Last.FM API library for Go

## Building

Building is quite easy, just make sure you have [Go](https://golang.org/) installed, and run `go build` You should be able to run the compiled executable after making required changes to `config.json` for your bot

## Contributing

Please refer to [CONTRIBUTING.md](CONTRIBUTING.md) for more info

## Planned Features

Theres a fair bit I want to do with this bot, I'll add a list of features here when I think of them, and if I find them an obtainable goal
